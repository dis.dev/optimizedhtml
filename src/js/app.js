'use strict';

document.addEventListener("DOMContentLoaded", function() {

    // SVG IE11 support
    svg4everybody();
    
    // Mask input
    $("input[name='phone']").mask("+7(999) 999-9999");

    // Mobile Nav Toggle
    $('.nav-toggle').on('click', function(e){
        e.preventDefault();
        $('.page').toggleClass('nav-open');
    });

    // Modal 
    $('.btn-modal').fancybox({
        autoFocus: false,
    });
    
});
